pub mod cli;
use clap::Parser;
use cli::Cli;
use player::is_active;

use std::mem;

pub mod farm_filter;
pub mod player;
pub mod sql_line;

use std::error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;
use std::sync::{mpsc, Arc, Mutex};
use std::thread;
use std::time::Instant;

fn read_file(path: &str, sender: mpsc::Sender<String>) -> Result<(), Box<dyn error::Error>> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    for line in reader.lines() {
        sender.send(line?)?;
    }

    Ok(())
}

//todo remove
fn read_file_old(path: &str) -> Result<Vec<String>, Box<dyn error::Error>> {
    let file = File::open(path)?;
    let reader = BufReader::new(file);
    let mut lines = Vec::new();

    for line in reader.lines() {
        lines.push(line?);
    }

    Ok(lines)
}

pub struct ParsedData {
    sql_lines: Vec<sql_line::SqlLine>,
    max_user_id: u32,
    max_aliance_id: u32,
}

// impl ParsedData {
//     fn new() -> Self {
//         Self {
//             sql_lines: Vec::new(),
//             max_user_id: 0,
//             max_aliance_id: 0,
//         }
//     }
// }

// fn parse_lines(lines: Vec<String>) -> Result<ParsedData, Box<dyn error::Error>> {
//     let mut parsed_data = ParsedData::new();

//     for l in lines.iter() {
//         let village = sql_line::SqlLine::parse(l)?;

//         if village.user_id > parsed_data.max_user_id {
//             parsed_data.max_user_id = village.user_id;
//         }
//         if village.aliance_id > parsed_data.max_aliance_id {
//             parsed_data.max_aliance_id = village.aliance_id;
//         }

//         parsed_data.sql_lines.push(village);
//     }

//     Ok(parsed_data)
// }

#[derive(Clone)]
struct Aliance {
    active_count: u32,
    inactive_count: u32,
}

impl Aliance {
    fn new() -> Self {
        Aliance {
            active_count: 0,
            inactive_count: 0,
        }
    }
}

fn find_inactives(
    players_day1: Vec<Option<player::Player>>,
    mut players_day2: Vec<Option<player::Player>>,
    max_aliance_id: u32,
) -> Vec<player::Player> {
    let mut inactive_players = Vec::new();
    let mut aliances = vec![Aliance::new(); max_aliance_id as usize + 1];
    for pair in players_day1.iter().zip(players_day2.iter_mut()) {
        if let (Some(day1), Some(day2)) = pair {
            if is_active(day1, day2) {
                aliances[day2.aliance_id as usize].active_count += 1;
            } else {
                aliances[day2.aliance_id as usize].inactive_count += 1;
                inactive_players.push(mem::take(day2));
            }
        }
    }

    println!("Number of inactive players: {}", inactive_players.len());

    let mut save_to_farm = Vec::new();
    for player in inactive_players.iter_mut() {
        let active_player_count = aliances[player.aliance_id as usize].active_count;

        //todo
        if player.aliance_id == 0 || active_player_count == 0 {
            save_to_farm.push(mem::take(player));
        }
    }

    println!("Safe to farm: {}", save_to_farm.len());

    save_to_farm
}

fn extract_villages(mut players: Vec<player::Player>) -> Vec<player::village::Village> {
    let mut villages = Vec::new();

    for player in players.iter_mut() {
        villages.append(&mut player.villages);
    }

    villages
}

fn filter_and_display(
    villages: Vec<player::village::Village>,
    args: &cli::Cli,
) -> Result<(), Box<dyn error::Error>> {
    let mut file = File::create(&args.output_file)?;

    let mut count = 0;
    for village in villages.iter() {
        if village.pop >= args.min_pop && (args.only_capitals == 0 || village.is_capital) {
            write!(
                file,
                "({}|{}) {}fields {}pop ",
                village.x,
                village.y,
                village.distance_from(args.x, args.y, args.map_width),
                village.pop
            )?;
            writeln!(
                file,
                "{}/position_details.php?x={}&y={}",
                args.domain, village.x, village.y
            )?;
            count += 1;
        }
    }

    println!("{} new farms found", count);

    Ok(())
}

fn main() {
    //measure execution time
    let before = Instant::now();

    let args = Cli::parse();

    // let lines1 = read_file(&args.input_file1).expect("Error while reading the first input file!");
    // let lines2 = read_file(&args.input_file2).expect("Error while reading the second input file!");

    // let parsed_day1 = parse_lines(lines1).expect("Error while parsing the first input file!");
    // let parsed_day2 = parse_lines(lines2).expect("Error while parsing the second input file!");

    let (sender1, receiver1) = mpsc::channel();
    let (sender2, receiver2) = mpsc::channel();
    let mut sql_lines_day1 = Vec::new();
    let mut sql_lines_day2 = Vec::new();

    let file1 = args.input_file1.clone();
    let handle1 = thread::spawn(move || {
        read_file(&file1, sender1).expect("Error while reading the first input file!")
    });

    let max_user_id1 = Arc::new(Mutex::new(0u32));
    let max_uid1 = max_user_id1.clone();
    let max_aliance_id1 = Arc::new(Mutex::new(0u32));
    let max_aid1 = max_aliance_id1.clone();

    let handle2 = thread::spawn(move || {
        while let Ok(raw_line) = receiver1.recv() {
            let parsed_line = sql_line::SqlLine::parse(&raw_line);
            if let Ok(line) = parsed_line {
                let mut uid = max_uid1.lock().expect("Unable to get a lock");
                let mut aid = max_aid1.lock().expect("Unable to get a lock");

                if line.user_id > *uid {
                    *uid = line.user_id;
                }
                if line.aliance_id > *aid {
                    *aid = line.aliance_id;
                }
                sql_lines_day1.push(line);
            }
        }

        sql_lines_day1
    });

    let file2 = args.input_file2.clone();
    let handle3 = thread::spawn(move || {
        read_file(&file2, sender2).expect("Error while reading the second input file!")
    });

    let max_user_id2 = Arc::new(Mutex::new(0u32));
    let max_uid2 = max_user_id2.clone();
    let max_aliance_id2 = Arc::new(Mutex::new(0u32));
    let max_aid2 = max_aliance_id2.clone();

    let handle4 = thread::spawn(move || {
        while let Ok(raw_line) = receiver2.recv() {
            let parsed_line = sql_line::SqlLine::parse(&raw_line);
            if let Ok(line) = parsed_line {
                let mut uid = max_uid2.lock().expect("Unable to get a lock");
                let mut aid = max_aid2.lock().expect("Unable to get a lock");

                if line.user_id > *uid {
                    *uid = line.user_id;
                }
                if line.aliance_id > *aid {
                    *aid = line.aliance_id;
                }
                sql_lines_day2.push(line);
            }
        }

        sql_lines_day2
    });

    handle1.join().unwrap();
    let sql_lines_day1 = handle2.join().unwrap();
    handle3.join().unwrap();
    let sql_lines_day2 = handle4.join().unwrap();

    let parsed_day1;
    {
        let max_uid1 = *max_user_id1.lock().unwrap();
        let max_aid1 = *max_aliance_id1.lock().unwrap();
        parsed_day1 = ParsedData {
            sql_lines: sql_lines_day1,
            max_user_id: max_uid1,
            max_aliance_id: max_aid1,
        };
    }

    let parsed_day2;
    {
        let max_uid2 = *max_user_id2.lock().unwrap();
        let max_aid2 = *max_aliance_id2.lock().unwrap();
        parsed_day2 = ParsedData {
            sql_lines: sql_lines_day2,
            max_user_id: max_uid2,
            max_aliance_id: max_aid2,
        };
    }

    // let parsed_day1 =
    //     thread::spawn(|| parse_lines(lines1).expect("Error while parsing the first input file!"));

    // lines2.join().unwrap();

    // let parsed_day2 =
    //     thread::spawn(|| parse_lines(lines2).expect("Error while parsing the second input file!"));

    // let parsed_day1 = parsed_day1.join().unwrap();
    // let parsed_day2 = parsed_day2.join().unwrap();

    if parsed_day1.max_user_id > parsed_day2.max_user_id
        || parsed_day1.max_aliance_id > parsed_day2.max_aliance_id
    {
        panic!("Invalid input! Please check that the older of the input files comes first");
    }

    let max_aid = parsed_day2.max_aliance_id;
    // let players1 = player::get_players(parsed_day1);
    // let players2 = player::get_players(parsed_day2);
    let players1 = thread::spawn(|| player::get_players(parsed_day1));
    let players2 = thread::spawn(|| player::get_players(parsed_day2));

    let players1 = players1.join().unwrap();
    let players2 = players2.join().unwrap();

    let inactive_players = find_inactives(players1, players2, max_aid);

    let mut inactive_villages = extract_villages(inactive_players);

    if !args.existing_farmlist_file.is_empty() {
        println!("FL file is not empty. Parsing...");
        let fl_lines = read_file_old(&args.existing_farmlist_file)
            .expect("Something went wrong with reading the farmlist file...");
        inactive_villages =
            farm_filter::parse_fl(fl_lines, inactive_villages, args.x, args.y, args.map_width);
    }

    //todo make into 2 separate closures
    let comparator = |x: &player::village::Village, y: &player::village::Village| {
        if args.order_by_size == 1 {
            y.pop.partial_cmp(&x.pop).unwrap()
        } else {
            x.distance_from(args.x, args.y, args.map_width)
                .partial_cmp(&y.distance_from(args.x, args.y, args.map_width))
                .unwrap()
        }
    };

    inactive_villages.sort_unstable_by(comparator);

    filter_and_display(inactive_villages, &args).unwrap();

    //execution time measurement
    println!("Elapsed time: {:.2?}", before.elapsed());
}
