use crate::sql_line;
use std::mem;
pub mod village;

#[derive(Default, Debug)]
pub struct Player {
    pub user_id: u32,
    pub user_name: String,
    pub aliance_id: u32,
    pub aliance_name: String,
    pub villages: Vec<village::Village>,
}

impl Player {
    fn new(sql_line: &mut sql_line::SqlLine) -> Self {
        Self {
            user_id: sql_line.user_id,
            user_name: mem::take(&mut sql_line.user_name),
            aliance_id: sql_line.aliance_id,
            aliance_name: mem::take(&mut sql_line.aliance_name),
            villages: Vec::new(),
        }
    }
}

impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        self.user_id == other.user_id
    }
}

pub fn is_active(p1: &Player, p2: &Player) -> bool {
    if p1.villages.len() < p2.villages.len() {
        return true;
    }
    for v1 in p1.villages.iter() {
        for v2 in p2.villages.iter() {
            if *v1 == *v2 && v1.pop < v2.pop {
                return true;
            }
        }
    }

    false
}

pub fn get_players(mut data: crate::ParsedData) -> Vec<Option<Player>> {
    let mut players: Vec<Option<Player>> = Vec::new();
    players.resize_with(data.max_user_id as usize + 1, || None);

    for sql_record in data.sql_lines.iter_mut() {
        let village = village::Village::new(sql_record);

        let index: usize = sql_record.user_id as usize;
        let player = &mut players[index];

        match player {
            Some(p) => p.villages.push(village),
            None => {
                let mut p = Player::new(sql_record);
                p.villages.push(village);
                //mem::replace(&mut players[index], Some(p));
                players[index] = Some(p);
            }
        }
    }

    players
}
