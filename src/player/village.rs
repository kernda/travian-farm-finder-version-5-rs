use crate::sql_line;
use std::mem;

#[derive(Debug, Default)]
pub struct Village {
    pub map_id: u32,
    pub x: i32,
    pub y: i32,
    pub tribe: u32,
    pub village_id: u32,
    pub village_name: String,
    pub pop: u32,
    pub region: String,
    pub is_capital: bool,
    pub is_city: bool,
    pub victory_points: u32,
}

impl Village {
    pub fn new(sql_line: &mut sql_line::SqlLine) -> Self {
        Self {
            map_id: sql_line.map_id,
            x: sql_line.x,
            y: sql_line.y,
            tribe: sql_line.tribe,
            village_id: sql_line.village_id,
            village_name: mem::take(&mut sql_line.village_name),
            pop: sql_line.pop,
            region: mem::take(&mut sql_line.region),
            is_capital: sql_line.is_capital,
            is_city: sql_line.is_city,
            victory_points: sql_line.victory_points,
        }
    }

    //              (22|6) -> (-200|-200)

    pub fn distance_from(&self, x: i32, y: i32, map_width: u32) -> f64 {
        let map_width = map_width as i32 + 1;
        let mut dx = i32::abs(self.x - x);
        let mut dy = i32::abs(self.y - y);

        if dx >= map_width / 2 {
            dx = map_width - i32::abs(self.x - x);
        }
        if dy >= map_width / 2 {
            dy = map_width - i32::abs(self.y - y);
        }

        (f64::powi(dx as f64, 2) + f64::powi(dy as f64, 2)).sqrt()
    }
}

impl PartialEq for Village {
    fn eq(&self, other: &Self) -> bool {
        self.village_id == other.village_id
    }
}
