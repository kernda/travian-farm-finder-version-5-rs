use crate::player::village;
use std::mem;

pub fn float_eq(x: f64, y: f64) -> bool {
    x < y + 0.1 && x > y - 0.1
}

pub fn parse_fl(
    fl_lines: Vec<String>,
    mut villages: Vec<village::Village>,
    x: i32,
    y: i32,
    map_width: u32,
) -> Vec<village::Village> {
    let mut farm_vec = Vec::new();
    let mut filtered_out_count = 0;

    for village in villages.iter_mut() {
        let mut farm_identified = false;
        for line in fl_lines.iter() {
            let l = line.trim_start();

            if l.starts_with(village.village_name.as_str()) {
                let words: Vec<_> = l[village.village_name.len()..].split_whitespace().collect();
                if words.len() > 2 {
                    let pop = words[0].parse::<u32>();
                    let distance = words[1].parse::<f64>();

                    if let (Ok(p), Ok(d)) = (pop, distance) {
                        if p == village.pop && float_eq(village.distance_from(x, y, map_width), d) {
                            farm_identified = true;
                            break;
                        }
                    }
                }
            }
        }
        if !farm_identified {
            farm_vec.push(mem::take(village));
        } else {
            filtered_out_count += 1;
        }
    }

    println!("{filtered_out_count} farms filtered out");

    farm_vec
}
