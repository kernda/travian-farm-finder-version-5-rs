use clap::Parser;

#[derive(Parser, Debug)]
pub struct Cli {
    pub input_file1: String,
    pub input_file2: String,
    pub output_file: String,
    pub domain: String,
    pub x: i32,
    pub y: i32,
    #[clap(default_value_t = 0)]
    pub min_pop: u32,
    #[clap(default_value_t = 0)]
    pub only_capitals: i32, //TODO use bool
    #[clap(default_value_t = 0)]
    pub order_by_size: i32, //TODO use bool
    #[clap(default_value_t = 400)]
    pub map_width: u32,
    #[clap(default_value_t = String::new())]
    pub existing_farmlist_file: String,
}
