use std::error;

#[derive(Debug, Default)]
pub struct SqlLine {
    pub map_id: u32,
    pub x: i32,
    pub y: i32,
    pub tribe: u32,
    pub village_id: u32,
    pub village_name: String,
    pub user_id: u32,
    pub user_name: String,
    pub aliance_id: u32,
    pub aliance_name: String,
    pub pop: u32,
    pub region: String,
    pub is_capital: bool,
    pub is_city: bool,
    pub victory_points: u32,
}

impl SqlLine {
    //INSERT INTO `x_world` VALUES (1,-200,200,5,1,'Natars',1,'Natars',0,'',8,'Caledonia',TRUE,FALSE,0);
    pub fn parse(raw_data: &str) -> Result<SqlLine, Box<dyn error::Error>> {
        let mut values: Vec<&str> = Vec::new();
        let data = &raw_data[30..raw_data.len() - 2];

        let mut is_name = false;
        let mut lower_index = 0;
        let mut upper_index = 0;

        for (current_index, c) in data.char_indices() {
            match (c, is_name) {
                ('\'', false) => {
                    lower_index = current_index + 1;
                    is_name = true;
                }
                ('\'', true) => {
                    upper_index = current_index;
                    is_name = false;
                }
                (',', false) => {
                    values.push(&data[lower_index..upper_index]);
                    lower_index = current_index + 1;
                    upper_index = lower_index;
                }
                _ => {
                    upper_index += c.len_utf8();
                }
            }
        }

        values.push(&data[lower_index..upper_index]);

        let map_id = values[0].parse()?;
        let x = values[1].parse()?;
        let y = values[2].parse()?;
        let tribe = values[3].parse()?;
        let village_id = values[4].parse()?;
        let village_name = values[5].to_string();
        let user_id = values[6].parse()?;
        let user_name = values[7].to_string();
        let aliance_id = values[8].parse()?;
        let aliance_name = values[9].to_string();
        let pop = values[10].parse()?;
        let region = values[11].to_string();
        let is_capital = match values[12].chars().next() {
            Some('T') => true,
            Some('F') => false,
            _ => panic!("is_capital value missing in the sql file"), //TODO return error instead of panicking
        };
        let is_city = match values[13].chars().next() {
            Some('T') => true,
            Some('F') => false,
            Some('N') => false, //NULL - on normal servers cities do not exist
            _ => panic!("is_city value missing in the sql file"), //TODO return error instead of panicking
        };

        //let victory_points = values[14].parse()?;
        let victory_points = values[14].parse();
        let victory_points = match victory_points {
            Ok(val) => val,
            _ => {
                if values[14] != "NULL" {
                    panic!("Victory points ERROR: Not an integer, not NULL");
                }

                0
            }
        };

        Ok(SqlLine {
            map_id,
            x,
            y,
            tribe,
            village_id,
            village_name,
            user_id,
            user_name,
            aliance_id,
            aliance_name,
            pop,
            region,
            is_capital,
            is_city,
            victory_points,
        })
    }
}
