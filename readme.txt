This program serves as a tool to find inactive players in online game Travian.
It compares two .sql files containing thousands of "INSERT INTO" statements. Each statement represents a single villages on the map.
After analysing the files, it creates an output file containing all the players who are inactive.